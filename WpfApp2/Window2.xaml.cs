﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;

namespace WpfApp2
{
    public partial class Window2 : System.Windows.Window
    {
        public Window2()
        {
            InitializeComponent();
        }

        private void FullIST(object sender, RoutedEventArgs e)
        {
            Hide();
            Window3 win3 = new Window3("all");
            win3.Show();
        }

        private void ShortIST(object sender, RoutedEventArgs e)
        {
            Hide();
            Window3 win3 = new Window3("short");
            win3.Show();
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Update(object sender, RoutedEventArgs e)
        {
            List<object> myList = new List<object>();
            List<object> newList = new List<object>();

            string myFile = @"c:\thrlist.xlsx";
            string link = @"https://bdu.fstec.ru/documents/files/thrlist.xlsx";
            string newFile = @"c:\Newthrlist.xlsx";
            WebClient webClient = new WebClient();
            webClient.DownloadFileAsync(new Uri(link), newFile);

            Excel.Application excelApp = new Excel.Application();
            Workbook excelBook = excelApp.Workbooks.Open(myFile);
            Worksheet excelSheet = (Worksheet)excelBook.Worksheets.get_Item(1);
            Range excelRange = excelSheet.UsedRange;
            var lastCell = excelSheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell);

            Excel.Application newExcelApp = new Excel.Application();
            Workbook newExcelBook = newExcelApp.Workbooks.Open(newFile);
            Worksheet newExcelSheet = (Worksheet)newExcelBook.Worksheets.get_Item(1);
            Range newExcelRange = newExcelSheet.UsedRange;
            var newLastCell = newExcelSheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell);

            myList = Class1.List(lastCell, excelSheet, "all");
            newList = Class1.List(newLastCell, newExcelSheet, "all");


            int count = 0;
            for (int i = 0; i <= lastCell.Row-3; i++)
            {
                string a = myList[i].ToString();
                string b = newList[i].ToString();
                if (a != b)
                {
                    count += 1;
                }
            }
            if (count == 0)
            {
                MessageBox.Show("Без изменений");
            }
            else
            {
                MessageBox.Show($"{count} записей изменено");
                for (int i = 0; i <= lastCell.Row - 3; i++)
                {
                    string a = myList[i].ToString();
                    string b = newList[i].ToString();
                    if (a != b)
                    {
                        MessageBox.Show(a + " --- " + b);
                    }
                }
            }
        }


    }
}
