﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace WpfApp2
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Begin(object sender, RoutedEventArgs e)
        {
            Hide();
            string file = @"c:\thrlist.xlsx";
            if (File.Exists(file))
            {
                Window2 win2 = new Window2();
                win2.Show();
            }
            else
            {
                Window1 win1 = new Window1();
                win1.Show();
            }
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
