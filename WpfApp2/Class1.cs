﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;

namespace WpfApp2
{
    class Class1
    {
        public static List<Object>  List(Range lastCell, Worksheet excelSheet, string ch)
        {

            List<object> list = new List<object>();
            Threat thr;
            if (ch == "all")
            {
                for (int i = 3; i <= lastCell.Row; i++)
                {
                    thr = new Threat();
                    thr.ID = excelSheet.Cells[i, 1].Text.ToString();
                    thr.Name = excelSheet.Cells[i, 2].Text.ToString();
                    thr.Inf = excelSheet.Cells[i, 3].Text.ToString();
                    thr.Source = excelSheet.Cells[i, 4].Text.ToString();
                    thr.Obj = excelSheet.Cells[i, 5].Text.ToString();
                    thr.Conf = excelSheet.Cells[i, 6].Text.ToString();
                    thr.Int = excelSheet.Cells[i, 7].Text.ToString();
                    thr.Acc = excelSheet.Cells[i, 8].Text.ToString();
                    thr.DateIn = excelSheet.Cells[i, 9].Text.ToString();
                    thr.DateLast = excelSheet.Cells[i, 10].Text.ToString();

                    list.Add(thr);
                }
            }
            else if (ch == "short")
            {
                for (int i = 3; i <= lastCell.Row; i++)
                {
                    thr = new Threat();
                    thr.ID = "УБИ." + excelSheet.Cells[i, 1].Text.ToString();
                    thr.Name = excelSheet.Cells[i, 2].Text.ToString();
                    
                    list.Add(thr);
                }
            }
            return list;
        }

    }
    public class Threat
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Inf { get; set; }
        public string Source  { get; set; }
        public string Obj  { get; set; }
        public string Conf { get; set; }
        public string Int { get; set; }
        public string Acc { get; set; }
        public string DateIn { get; set; }
        public string DateLast { get; set; }

        public override string ToString()
        {
            return ID + " | " + Name + " | " + Inf + " | " + Source + " | " + Obj + " | " + Conf + " | " + Int + " | " + Acc + " | " + DateIn + " | " + DateLast;
        }
    }

}
