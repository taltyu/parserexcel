﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace WpfApp2
{
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void Download(object sender, RoutedEventArgs e)
        {
            string link = @"https://bdu.fstec.ru/documents/files/thrlist.xlsx";
            string file = @"c:\thrlist.xlsx";
            WebClient webClient = new WebClient();
            webClient.DownloadFileAsync(new Uri(link), file);
            if (File.Exists(file))
            {
                MessageBox.Show("База успешно загружена");
            }
            else
            {
                MessageBox.Show("Ошибка");
            }
            Close();
            MainWindow win = new MainWindow();
            win.Show();
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            Close();
            MainWindow win = new MainWindow();
            win.Show();
        }
    }
}
